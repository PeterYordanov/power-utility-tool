#ifndef BASEEXCEPTION_HPP
#define BASEEXCEPTION_HPP

#include <exception>
#include <string>

class BaseException : public std::exception
{
private:
    std::string m_message;

public:
    BaseException(const std::string& message = "")
        : m_message(message)
    {
    }

    const char* what() const noexcept
    {
        return m_message.c_str();
    }
};


#endif // BASEEXCEPTION_HPP
