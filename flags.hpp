#ifndef FLAGS_HPP
#define FLAGS_HPP

#include <type_traits>

template<typename T>
struct Flags {
    using UT = std::underlying_type<T>;
    UT t;
    constexpr bool isSet(UT u) const { return static_cast<bool>(*this & u); }
    template<typename U>
        static constexpr Flags cast(U u) { return {static_cast<UT>(u)}; }
    constexpr Flags &flip() { return *this = ~*this; }
    constexpr Flags &set(UT u) { return *this = *this | u; }
    constexpr Flags &set(UT u, bool b);
    constexpr Flags &clear(UT u) { return *this = *this & ~Flags{u}; }
    constexpr explicit operator bool() const { return t; }
    constexpr Flags operator~() const { return Flags::cast(~this->t); }
    constexpr Flags operator-() const { return Flags::cast(-this->t); }
    constexpr Flags operator&(UT u) const { return Flags::cast(this->t & u); }
    constexpr Flags operator|(UT u) const { return Flags::cast(this->t | u); }
    constexpr Flags operator^(UT u) const { return Flags::cast(this->t ^ u); }
    constexpr Flags &operator&=(UT u) { return *this = *this & Flags{u}; }
    constexpr Flags &operator|=(UT u) { return *this = *this | Flags{u}; }
    constexpr Flags &operator^=(UT u) { return *this = *this ^ Flags{u}; }
    constexpr Flags operator&(const Flags &f) const { return *this & f.t; }
    constexpr Flags operator|(const Flags &f) const { return *this | f.t; }
    constexpr Flags operator^(const Flags &f) const { return *this ^ f.t; }
    constexpr Flags &operator&=(const Flags &f) { return *this = *this & f.t; }
    constexpr Flags &operator|=(const Flags &f) { return *this = *this | f.t; }
    constexpr Flags &operator^=(const Flags &f) { return *this = *this ^ f.t; }
};

template<typename T> constexpr Flags<T> &Flags<T>::set(UT u, bool b)
{ return *this ^= (*this ^ -Flags{b}) & u; }

#endif // FLAGS_HPP
