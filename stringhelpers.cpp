#include "stringhelpers.hpp"

std::wstring toLower(std::wstring source)
{
    std::transform(source.begin(), source.end(), source.begin(), [](unsigned char c){ return std::tolower(c); });
    return source;
}

std::wstring toUpper(std::wstring source)
{
    std::transform(source.begin(), source.end(), source.begin(), [](unsigned char c){ return std::toupper(c); });
    return source;
}

std::wstring trimStart(std::wstring source, const std::wstring& trimChars)
{
    return source.erase(0, source.find_first_not_of(trimChars));
}

std::wstring trimEnd(std::wstring source, const std::wstring& trimChars)
{
    return source.erase(source.find_last_not_of(trimChars) + 1);
}

std::wstring trim(std::wstring source, const std::wstring& trimChars)
{
    return trimStart(trimEnd(source, trimChars), trimChars);
}

bool startsWith(const std::wstring& source, const std::wstring& value)
{
    if (source.length() < value.length())
        return false;
    else
        return source.compare(0, value.length(), value) == 0;
}

bool endsWith(const std::wstring& source, const std::wstring& value)
{
    if (source.length() < value.length())
        return false;
    else
        return source.compare(source.length() - value.length(), value.length(), value) == 0;
}

bool isEmptyOrWhiteSpace(const std::wstring& source)
{
    if (source.length() == 0)
        return true;
    else {
        for (std::size_t index = 0; index < source.length(); index++) {
            if (!std::isspace(source[index]))
                return false;
        }

        return true;
    }
}

std::vector<std::wstring> split(const std::wstring& source, wchar_t delimiter)
{
    std::vector<std::wstring> output;
    std::wistringstream ss(source);
    std::wstring nextItem;

    while (std::getline(ss, nextItem, delimiter)) {
        output.push_back(nextItem);
    }

    return output;
}

std::wstring replace(std::wstring source, const std::wstring& find, const std::wstring& replace)
{
    std::size_t pos = 0;
    while ((pos = source.find(find, pos)) != std::wstring::npos) {
        source.replace(pos, find.length(), replace);
        pos += replace.length();
    }
    return source;
}
