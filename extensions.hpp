#ifndef EXTENSIONS_HPP
#define EXTENSIONS_HPP

#include <iostream>

namespace xps::extensions {
    template <typename _CharT, typename _Traits>
    inline std::basic_ostream<_CharT, _Traits> &
    tab(std::basic_ostream<_CharT, _Traits> &__os) {
        return __os.put(__os.widen('\t'));
    }

    template <typename _CharT, typename _Traits>
    inline std::basic_ostream<_CharT, _Traits> &
    newl(std::basic_ostream<_CharT, _Traits> &__os) {
        return __os.put(__os.widen('\n'));
    }

    template<typename LHS, typename RHS>
    auto coalesce(LHS lhs, RHS rhs) ->
    typename std::remove_reference<decltype(lhs())>::type&&
    {
        auto&& initialValue = lhs();
        if (initialValue)
            return std::move(initialValue);
        else
            return std::move(rhs());
    }

    template<typename LHS, typename RHS, typename ...RHSs>
    auto coalesce(LHS lhs, RHS rhs, RHSs ...rhss) ->
    typename std::remove_reference<decltype(lhs())>::type&&
    {
        auto&& initialValue = lhs();
        if (initialValue)
            return std::move(initialValue);
        else
            return std::move(coalesce(rhs, rhss...));
    }

    #define COALESCE(x) (::coalesce_impl::coalesce([&](){ return ( x ); }))
    #define IF_NULL     ); }, [&](){ return (

    class NonCopyable
    {
    public:
    #if __cplusplus >= 201103L
        NonCopyable() = default;
        NonCopyable(const NonCopyable&) = delete;
        NonCopyable& operator=(const NonCopyable&) = delete;
    #else
    private:
        inline NonCopyable(){}
        NonCopyable(const NonCopyable&);
        NonCopyable& operator=(const NonCopyable&);
    #endif
    };

    class NonMovable
    {
    #if __cplusplus >= 201103L
    public:
        NonMovable() = default;
        NonMovable(NonMovable&&) = delete;
        NonMovable& operator=(NonMovable&&) = delete;
    #endif //__cplusplus >= 201103L
    //Pre-C++11 does not support rvalue references, hence move semantics as well
    };
}

#endif // EXTENSIONS_HPP
