#include "powerutilitytools.hpp"

int main(int argc, char* argv[]) {
    ultralight::RefPtr<ultralight::App> app = ultralight::App::Create();

    ultralight::RefPtr<ultralight::Window> window = ultralight::Window::Create(app->main_monitor(), WINDOW_WIDTH, WINDOW_HEIGHT,
      false, ultralight::kWindowFlags_Resizable /*| ultralight::kWindowFlags_Borderless*/);

    window->SetTitle("Power Utility Tool");

    app->set_window(*window.get());

    PowerUtilityTool app_(window, app);
    app_.Run();

    return 0;
}
