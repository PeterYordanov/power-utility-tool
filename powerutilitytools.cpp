#include "powerutilitytools.hpp"

PowerUtilityTool::PowerUtilityTool(ultralight::RefPtr<ultralight::Window> win,
                                       ultralight::RefPtr<ultralight::App> app) {
    m_window = win;
    m_app = app;

    m_sidebar_panel = ultralight::Overlay::Create(*m_window.get(), 1, 1, 0, 0);
    m_main_content = ultralight::Overlay::Create(*m_window.get(), 1, 1, 0, 0);

    OnResize(m_window->width(), m_window->height());

    m_sidebar_panel->view()->LoadURL("file:///sidebar_panel.html");
    m_main_content->view()->LoadURL("file:///bulk_rename_content.html");

    m_window->set_listener(this);
    m_sidebar_panel->view()->set_load_listener(this);
    m_main_content->view()->set_load_listener(this);

    m_sidebar_panel->view()->set_view_listener(this);
    m_main_content->view()->set_view_listener(this);
}

void PowerUtilityTool::OnResize(std::uint32_t width, std::uint32_t height) {
    std::uint32_t left_pane_width_px = m_window->DeviceToPixels(LEFT_PANE_WIDTH);
    m_sidebar_panel->Resize(left_pane_width_px, height);

    int right_pane_width = (int)width - left_pane_width_px;

    right_pane_width = right_pane_width > 1 ? right_pane_width: 1;

    m_main_content->Resize((uint32_t)right_pane_width, height);

    m_sidebar_panel->MoveTo(0, 0);
    m_main_content->MoveTo(left_pane_width_px, 0);
}


ultralight::JSValue PowerUtilityTool::HardwareInfoImpl(const ultralight::JSObject& thisObject, const ultralight::JSArgs& args) {
    std::string ramGB = wStringToString(toString(xps::hardware::memory::getRAMGB()));
    std::string pageSize = wStringToString(toString(xps::hardware::memory::getPageSize()));
    std::string brandName = xps::hardware::cpu::getCPUBrandName();

    std::string cores = wStringToString(toString(xps::hardware::cpu::getCores()));
    std::string logicalCores = wStringToString(toString(xps::hardware::cpu::getLogicalCores()));
    std::string vendorName = xps::hardware::cpu::getVendorName();

    std::string platformName = xps::hardware::system_info::getPlatformName();
    std::string userName = xps::hardware::system_info::getUserName();
    std::string hostName = xps::hardware::system_info::getHostName();
    std::string bits = wStringToString(toString(xps::hardware::system_info::getSystemBits()));
    std::string nativeEndianness = xps::hardware::system_info::getNativeEndianness();

    std::string text;

    text += "RAM: " + ramGB + " GB\n"
            "Page size: " + pageSize + "\n"
            "CPU: " + brandName + "\n"
            //"Vendor Name: " + vendorName + "\n"
            "Cores: " + cores + "\n"
            "Logical Cores: " + logicalCores + "\n"
            "Platform Name: " + platformName + " " + bits + "-bit\n"
            "User Name: " + userName + "\n"
            "HostName: " + hostName + "\n"
            "Native Endianness: " + nativeEndianness + "\n";

    return ultralight::JSValue(text.c_str());
}

ultralight::JSValue PowerUtilityTool::StartBulkRenameImpl(const ultralight::JSObject& thisObject, const ultralight::JSArgs& args) {
    String pathUl = String(args[0].ToString());
    String regexUl = String(args[1].ToString());
    String replaceWithUl = String(args[2].ToString());

    std::string regex = std::string(regexUl.utf8().data());
    std::string path = std::string(pathUl.utf8().data());
    std::string replaceWith = std::string(replaceWithUl.utf8().data());

    for(auto& entry : getFiles(stringToWString(path))) {
        std::regex expression (regex);
        std::string path = entry.path().generic_string();

        if (std::regex_search (path, expression)) {
            std::string newPath = std::regex_replace(entry.path().generic_string(), expression, replaceWith);
            if(isFile(entry.path()) || isDirectory(entry.path())) {
                renamePath(entry.path(), stringToWString(newPath));
            }
        }
    }

    return ultralight::JSValue();
}

ultralight::JSValue PowerUtilityTool::ChangeContent(const ultralight::JSObject& thisObject, const ultralight::JSArgs& args) {

    String html_string = String(args[1].ToString());

    std::string buttonText = std::string(html_string.utf8().data());

    if(buttonText == "pdfEdit") {
        m_main_content->view()->LoadURL("file:///pdf_edit_content.html");
    } else if(buttonText == "imageResize") {
        m_main_content->view()->LoadURL("file:///image_resize_content.html");
    } else if(buttonText == "hardwareInfo") {
        m_main_content->view()->LoadURL("file:///hardware_info_content.html");
    } else if(buttonText == "bulkRename") {
        m_main_content->view()->LoadURL("file:///bulk_rename_content.html");
    } else if(buttonText == "pyordanov") {
        m_main_content->view()->LoadURL("file:///pyordanov.html");
    } else if(buttonText == "settings") {
        m_main_content->view()->LoadURL("file:///settings_content.html");
    } else if(buttonText == "about") {
        m_main_content->view()->LoadURL("file:///about_content.html");
    }

    return ultralight::JSValue();
}

void PowerUtilityTool::OnDOMReady(ultralight::View* caller,
                         uint64_t frame_id,
                         bool is_main_frame,
                         const ultralight::String& url) {

    ultralight::Ref<ultralight::JSContext> context = caller->LockJSContext();
    ultralight::SetJSContext(context.get());

    ultralight::JSObject global = ultralight::JSGlobalObject();
    global["HardwareInfoImpl"] = BindJSCallbackWithRetval(&PowerUtilityTool::HardwareInfoImpl);
    global["ChangeContent"] = BindJSCallbackWithRetval(&PowerUtilityTool::ChangeContent);
    global["StartBulkRenameImpl"] = BindJSCallbackWithRetval(&PowerUtilityTool::StartBulkRenameImpl);
 }
