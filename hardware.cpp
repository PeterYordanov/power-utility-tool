#include "hardware.hpp"
#include <bit>

#define XPS_INFO_BUFFER_SIZE 1767

#if defined(XPS_PLATFORM_LINUX)
    #include <unistd.h>
    #include <sys/sysinfo.h>
    #include <linux/sched.h>
    #include <stdlib.h>
    #include <cpuid.h>
    #include <limits.h>
#elif defined(XPS_PLATFORM_WINDOWS)
    #include <intrin.h>
    #include <Windows.h>
    #include <tchar.h>
    #include <malloc.h>
#endif

namespace xps::hardware {
    namespace memory {
        unsigned long long getRAMkb() {
            #if defined(XPS_PLATFORM_WINDOWS)
                unsigned long long RAM;
                GetPhysicallyInstalledSystemMemory(&RAM);
                return RAM;
            #elif defined(XPS_PLATFORM_LINUX)
                struct sysinfo info;
                sysinfo(&info);
                return info.totalram;
            #endif
        }

        int getRAMGB() {
            return static_cast<int>(floor(getRAMkb() * std::pow(10, -6)));
        }

        int getPageSize() {
            #if defined(XPS_PLATFORM_WINDOWS)
                SYSTEM_INFO info;
                GetSystemInfo(&info);
                return static_cast<int>(info.dwPageSize);
            #elif defined(XPS_PLATFORM_LINUX)
                return static_cast<int>(sysconf(_SC_PAGESIZE));
            #endif
        }
    }

    namespace cpu {
        int getCores() {
            #if defined(XPS_PLATFORM_WINDOWS)
                typedef DWORD (WINAPI* LPFN_GLPI)(PSYSTEM_LOGICAL_PROCESSOR_INFORMATION, PDWORD);

                int cores = 0;
                bool isDone = false;
                DWORD ret = 0;
                LPFN_GLPI glpi = (LPFN_GLPI)GetProcAddress(GetModuleHandle("kernel32"), "GetLogicalProcessorInformation");
                PSYSTEM_LOGICAL_PROCESSOR_INFORMATION ptr = NULL;
                DWORD byte_offset = 0;

                PSYSTEM_LOGICAL_PROCESSOR_INFORMATION buffer = NULL;

                while (!isDone) {
                    DWORD rc = glpi(buffer, &ret);

                    if (!rc) {
                        if (GetLastError() == ERROR_INSUFFICIENT_BUFFER) {
                            std::free(buffer);

                            buffer = (PSYSTEM_LOGICAL_PROCESSOR_INFORMATION)malloc(ret);
                        }
                    } else isDone = true;
                }

                ptr = buffer;

                while (byte_offset + sizeof(SYSTEM_LOGICAL_PROCESSOR_INFORMATION) <= ret) {
                    switch(ptr->Relationship) {
                    case RelationProcessorCore:
                        cores++;
                        break;
                    }

                    byte_offset += sizeof(SYSTEM_LOGICAL_PROCESSOR_INFORMATION);
                    ptr++;
                }

                std::free(buffer);

                return cores;
            #elif defined(XPS_PLATFORM_LINUX)
                int cores;
                FILE* cmd = popen("grep '^cpu cores' /proc/cpuinfo | head -n 1 | tail -c 2", "r");
                char buffer[sizeof(int)];
                fread(buffer, 1, sizeof(buffer), cmd);
                sscanf(buffer, "%u", &cores);
                pclose(cmd);
                return cores;
            #endif
        }

        int getLogicalCores() {
            #if defined(XPS_PLATFORM_WINDOWS)
                SYSTEM_INFO info;
                GetSystemInfo(&info);
                return static_cast<int>(info.dwNumberOfProcessors);
            #elif defined(XPS_PLATFORM_LINUX)
                return static_cast<int>(sysconf(_SC_NPROCESSORS_CONF));
            #endif
        }

        CacheLevels getCacheLevels() {
            CacheLevels cacheLevels;

            #if defined(XPS_PLATFORM_WINDOWS)
                typedef DWORD (WINAPI* LPFN_GLPI)(PSYSTEM_LOGICAL_PROCESSOR_INFORMATION, PDWORD);

                bool isDone = false;
                DWORD ret = 0;
                LPFN_GLPI glpi = (LPFN_GLPI)GetProcAddress(GetModuleHandle("kernel32"), "GetLogicalProcessorInformation");
                PSYSTEM_LOGICAL_PROCESSOR_INFORMATION ptr = NULL;
                DWORD byte_offset = 0;

                PSYSTEM_LOGICAL_PROCESSOR_INFORMATION buffer = NULL;

                while (!isDone) {
                    DWORD rc = glpi(buffer, &ret);

                    if (!rc) {
                        if (GetLastError() == ERROR_INSUFFICIENT_BUFFER) {
                            free(buffer);

                            buffer = (PSYSTEM_LOGICAL_PROCESSOR_INFORMATION)malloc(ret);
                        }
                    } else isDone = true;
                }

                ptr = buffer;

                while (byte_offset + sizeof(SYSTEM_LOGICAL_PROCESSOR_INFORMATION) <= ret) {

                    details::CacheLevel tempLevel;

                    switch(ptr->Relationship) {
                    case RelationCache:
                        PCACHE_DESCRIPTOR cache = &ptr->Cache;
                        if (cache->Level == 1 || cache->Level == 2 ||
                            cache->Level == 3 || cache->Level == 4) {
                            tempLevel.level = cache->Level;
                            tempLevel.size = static_cast<int>(cache->Size);
                            tempLevel.lineSize = cache->LineSize;
                            tempLevel.associativity = cache->Associativity;
                        }
                        break;
                    }

                    cacheLevels.push_back(tempLevel);

                    byte_offset += sizeof(SYSTEM_LOGICAL_PROCESSOR_INFORMATION);
                    ptr++;
                }
                std::free(buffer);

                return cacheLevels;
            #elif defined(XPS_PLATFORM_LINUX)

                cacheLevels.push_back(details::CacheLevel(1,
                                                          static_cast<int>(sysconf(_SC_LEVEL1_DCACHE_ASSOC)),
                                                          static_cast<int>(sysconf(_SC_LEVEL1_DCACHE_LINESIZE)),
                                                          static_cast<int>(sysconf(_SC_LEVEL1_DCACHE_SIZE))));

                cacheLevels.push_back(details::CacheLevel(2,
                                                          static_cast<int>(sysconf(_SC_LEVEL2_DCACHE_ASSOC)),
                                                          static_cast<int>(sysconf(_SC_LEVEL2_DCACHE_LINESIZE)),
                                                          static_cast<int>(sysconf(_SC_LEVEL2_DCACHE_SIZE))));

                cacheLevels.push_back(details::CacheLevel(3,
                                                          static_cast<int>(sysconf(_SC_LEVEL3_DCACHE_ASSOC)),
                                                          static_cast<int>(sysconf(_SC_LEVEL3_DCACHE_LINESIZE)),
                                                          static_cast<int>(sysconf(_SC_LEVEL3_DCACHE_SIZE))));

                cacheLevels.push_back(details::CacheLevel(4,
                                                          static_cast<int>(sysconf(_SC_LEVEL4_DCACHE_ASSOC)),
                                                          static_cast<int>(sysconf(_SC_LEVEL4_DCACHE_LINESIZE)),
                                                          static_cast<int>(sysconf(_SC_LEVEL4_DCACHE_SIZE))));


            #endif
        }

        void executeCPUID(details::Registers* regs, unsigned int level) {
            #if defined(XPS_PLATFORM_WINDOWS)
                __cpuid((int*)regs, (int)level);
            #elif defined(XPS_PLATFORM_LINUX)
                __get_cpuid(level, &regs->eax,
                                   &regs->ebx,
                                   &regs->ecx,
                                   &regs->edx);
            #endif
            /*
            asm volatile
            ("cpuid" : "=a" (regs[0]), "=b" (regs[1]), "=c" (regs[2]), "=d" (regs[3])
            : "a" (i), "c" (0));
            */
        }

        std::string getCPUBrandName() {
            details::Registers regs;

            char brand[XPS_CPU_BRAND_LENGTH];
            std::memset(brand, 0, sizeof(brand));
            executeCPUID(&regs, 0x80000002);
            std::memcpy(brand, &regs, sizeof(regs));
            executeCPUID(&regs, 0x80000003);
            std::memcpy(brand + 16, &regs, sizeof(regs));
            executeCPUID(&regs, 0x80000004);
            std::memcpy(brand + 32, &regs, sizeof(regs));

            return std::string(brand);
        }

        std::string getVendorName() {
            details::Registers regs;

            executeCPUID(&regs, 0);
            std::uint32_t vendor[XPS_CPU_REGISTERS];
            std::memset(vendor, 0, sizeof(vendor));
            vendor[0] = regs.ebx;
            vendor[1] = regs.edx;
            vendor[2] = regs.ecx;

            char vendorRet[XPS_CPU_VENDOR_LENGTH];

            return vendorRet;
        }

        bool isSSESupported() {
            details::Registers regs;
            executeCPUID(&regs, 1);
            return regs.edx & 0x02000000;
        }

        bool isSSE2Supported() {
            details::Registers regs;
            executeCPUID(&regs, 1);
            return regs.edx & 0x04000000;
        }

        bool isSSE3Supported() {
            details::Registers regs;
            executeCPUID(&regs, 1);
            return regs.edx & 0x0000001;
        }

        bool isSSE41Supported() {
            details::Registers regs;
            executeCPUID(&regs, 1);
            return regs.edx & 0x0008000;
        }

        bool isSSE42Supported() {
            details::Registers regs;
            executeCPUID(&regs, 1);
            return regs.edx & 0x00100000;
        }

        bool isAVXSupported() {
            details::Registers regs;
            executeCPUID(&regs, 1);
            return regs.edx & 0x1000000;
        }

        bool isAVX2Supported() {
            details::Registers regs;
            executeCPUID(&regs, 1);
            return regs.edx & 0x00000020;
        }

        bool isMMXSupported() {
            details::Registers regs;
            executeCPUID(&regs, 1);
            return regs.edx & 0x00000017;
        }

        bool isHyperThreadingSupported() {
            details::Registers regs;
            executeCPUID(&regs, 1);
            return regs.edx & 0x1000000;
        }

        bool isFPUSupported() {
            details::Registers regs;
            executeCPUID(&regs, 1);
            return regs.edx & 0x0000001;
        }

        bool isPCLMULQDQSupported() {
            details::Registers regs;
            executeCPUID(&regs, 1);
            return regs.edx & 0x0000002;
        }
    }

    namespace system_info {
        const char* getPlatformName() {
            return (XPS_PLATFORM_NAME == NULL) ? "Undefined" : XPS_PLATFORM_NAME;
        }

        const char* getHostName() {
            #if defined(XPS_PLATFORM_WINDOWS)
                TCHAR infoBuf[XPS_INFO_BUFFER_SIZE];
                DWORD bufCharCount = XPS_INFO_BUFFER_SIZE;
                GetComputerName(infoBuf, &bufCharCount);
                return infoBuf;
            #elif defined(XPS_PLATFORM_LINUX)
                char hostname[HOST_NAME_MAX];
                gethostname(hostname, HOST_NAME_MAX);
                return hostname;
            #endif
        }

        const char* getUserName() {
            #if defined(XPS_PLATFORM_WINDOWS)
                TCHAR infoBuf[XPS_INFO_BUFFER_SIZE];
                DWORD bufCharCount = XPS_INFO_BUFFER_SIZE;
                GetUserName(infoBuf, &bufCharCount);
                return infoBuf;
            #elif defined(XPS_PLATFORM_LINUX)
                char username[LOGIN_NAME_MAX];
                getlogin_r(username, LOGIN_NAME_MAX);
                return username;
            #endif
        }

        bool is64Bit() {
            return getSystemBits() == 64;
        }

        bool is32Bit() {
            return getSystemBits() == 32;
        }

        int getSystemBits() {
            return XPS_BITS;
        }

        bool isBigEndian() {
            return std::endian::native == std::endian::big;
        }

        bool isLittleEndian() {
            return std::endian::native == std::endian::little;
        }

        const char* getNativeEndianness() {
            return isLittleEndian() ? "Little" : "Big";
        }
    }
}
