#include "filesystem.hpp"

#if __cplusplus >= 201703L

void createDirectory(const std::wstring& path)
{
    if(!directoryExists(path)) {
        std::filesystem::create_directory(pathFromString(path));
    }
}

std::wstring combine(const std::wstring& path1, const std::wstring& path2)
{
    return (pathFromString(path1) / pathFromString(path2)).generic_wstring();
}

bool isPathRooted(const std::wstring& path)
{
    return pathFromString(path).has_root_path();
}

std::wstring getFullPath(const std::wstring& path)
{
    return std::filesystem::absolute(pathFromString(path)).generic_wstring();
}

std::wstring getFileName(const std::wstring& path)
{
    return std::filesystem::path(pathFromString(path))
                                                      .filename()
                                                      .generic_wstring();
}

std::wstring getDirectoryName(const std::wstring& path)
{
    return std::filesystem::path(pathFromString(path))
                                                     .parent_path()
                                                     .generic_wstring();
}

std::wstring getCurrentDirectory()
{
    return std::filesystem::current_path().generic_wstring();
}

void copyFile(const std::wstring& path1, const std::wstring& path2)
{
    std::filesystem::copy_file(pathFromString(path1), pathFromString(path2));
}

void renamePath(const std::wstring& path1, const std::wstring& path2)
{
    std::filesystem::rename(pathFromString(path1), pathFromString(path2));
}

#endif
