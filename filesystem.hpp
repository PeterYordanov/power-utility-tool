#ifndef FILESYSTEM_HPP
#define FILESYSTEM_HPP

#if __cplusplus >= 201703L

#include <string>
#include <filesystem>

void         createDirectory(const std::wstring& path);
std::wstring combine(const std::wstring& path1, const std::wstring& path2);
bool         isPathRooted(const std::wstring& path);
std::wstring getFullPath(const std::wstring& path);
std::wstring getFileName(const std::wstring& path);
std::wstring getDirectoryName(const std::wstring& path);
std::wstring getCurrentDirectory();
void         copyFile(const std::wstring& path1, const std::wstring& path2);
void         renamePath(const std::wstring& path1, const std::wstring& path2);

inline bool isDirectory(const std::wstring& path)
{
    return std::filesystem::is_directory(path);
}

inline bool isFile(const std::wstring& path)
{
    return std::filesystem::is_regular_file(path);
}

inline wchar_t preferredSeparator()
{
    return std::filesystem::path::preferred_separator;
}

inline std::filesystem::path pathFromString(const std::wstring& path)
{
    return std::filesystem::path(&path[0]);
}

inline bool fileExists(const std::wstring& path)
{
    return std::filesystem::is_regular_file(pathFromString(path));
}

inline bool directoryExists(const std::wstring& path)
{
    return std::filesystem::is_directory(pathFromString(path));
}

inline std::filesystem::directory_iterator getFiles(const std::wstring& path) {
    return std::filesystem::directory_iterator(path);
}

#endif

#endif // FILESYSTEM_HPP
