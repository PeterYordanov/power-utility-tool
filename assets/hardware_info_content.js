function GetHardwareInfo(evt) {
	var info = HardwareInfoImpl().split('\n');
	document.getElementById('ram').innerHTML = info[0];
	document.getElementById('pageSize').innerHTML = info[1];
	document.getElementById('brandName').innerHTML = info[2];
	document.getElementById('cores').innerHTML = info[3];
	document.getElementById('logicalCores').innerHTML = info[4];
	document.getElementById('platformName').innerHTML = info[5];
	document.getElementById('userName').innerHTML = info[6];
	document.getElementById('hostName').innerHTML = info[7];
	document.getElementById('endianness').innerHTML = info[8];
}